package com.playground.java.seventeen.sealed;

public final class Dog extends Animal implements Doctor {

    protected Dog(String name) {
        super(name);
    }

    @Override
    public int getMaxCheckupPrice() {
        return 3502;
    }

    @Override
    public int getMaxPeriodBetweenVisitsInDays() {
        return 175;
    }
}
