package com.playground.java.seventeen.sealed;

public final class Cat extends Animal implements Doctor {

    protected Cat(String name) {
        super(name);
    }

    @Override
    public int getMaxCheckupPrice() {
        return 1135;
    }

    @Override
    public int getMaxPeriodBetweenVisitsInDays() {
        return 200;
    }



}
