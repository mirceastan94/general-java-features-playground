package com.playground.java.seventeen.sealed;

public abstract sealed class Animal permits Dog, Cat {

    protected final String name;

    protected Animal(String name) {
        this.name = name;
    }

}
