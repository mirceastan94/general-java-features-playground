package com.playground.java.seventeen.sealed;

public sealed interface Doctor permits Dog, Cat {
    int getMaxCheckupPrice();

    default int getMaxPeriodBetweenVisitsInDays() {
        return 365;
    }
}
