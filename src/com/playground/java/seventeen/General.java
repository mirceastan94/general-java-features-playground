package com.playground.java.seventeen;

public class General {

    public static void main(String[] args) {
        createNewObject("testStr");
        createNewObject(3);
        createNewObject(32L);
        createNewObject(21.00);
        // Exception thrown with the below object
        createNewObject(new Object());
    }

    /**
     * This method expresses the new switch keyword implementation, similar to Kotlin and "instanceOf Pattern Matching"
     * @param objType
     */
    private static void createNewObject(Object objType) {
        System.out.println(switch (objType) {
            case Integer i -> String.format("int %d", i);
            case Long l -> String.format("long %d", l);
            case Double d -> String.format("double %f", d);
            case String s -> String.format("double %s", s);
            default -> throw new IllegalStateException("Unexpected value: " + objType);
        });
    }

}
