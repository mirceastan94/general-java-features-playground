package com.playground.java.seventeen.record;

import java.math.BigDecimal;

public record Car(String brand, BigDecimal price) {
    public static String UNKNOWN_BRAND = "Unknown car brand";
}
