package com.playground.java.reflection;

import com.playground.java.eight.DateTime;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

import static com.playground.utils.Utils.fillConsole;
import static com.playground.utils.Utils.printLine;

public class Reflection implements Serializable {

    String firstStr = "first";
    String secondStr = "first";
    int firstInt = 1;
    double firstDouble = 21.53;

    public Reflection() {
    }

    public Reflection(String firstStr, int firstInt) {
        this.firstStr = firstStr;
        this.firstInt = firstInt;
    }

    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException {

        final Class firstClass = Class.forName("com.playground.java.reflection.Reflection");
        printLine("Class name: " + firstClass.getName());

        final Reflection reflection = new Reflection();
        final Class<? extends Reflection> secondClass = reflection.getClass();
        printLine("Second class name: " + secondClass.getSimpleName());
        printLine("Second class super class simple name: " + secondClass.getSuperclass().getSimpleName());

        // a more direct way of creating a Class reference
        final Class<Reflection> thirdClass = Reflection.class;
        final Field[] fields = secondClass.getDeclaredFields();
        final Constructor[] constructors = secondClass.getDeclaredConstructors();
        final Method[] methods = thirdClass.getMethods();
        final Class<?>[] interfaces = thirdClass.getInterfaces();
        int modifiers = thirdClass.getModifiers();
        printLine("Class fields: " + Arrays.asList(fields));
        printLine("Class methods: " + Arrays.asList(methods));
        printLine("Class implemented interfaces: " + Arrays.asList(interfaces));
        printLine("Class modifiers: " + Modifier.toString(modifiers));

        fillConsole();

        for (final Field currentField : fields) {
            printLine("Current field's name: " + currentField.getName());
            printLine("Current field's type: " + currentField.getType());
        }

        fillConsole();

        for (final Method currentMethod : methods) {
            printLine("Current method's name: " + currentMethod.getName());
            printLine("Current method's type: " + currentMethod.getReturnType());
        }

        fillConsole();

        for (final Constructor<?> currentConstructor : constructors) {
            printLine("Current constructor's name: " + currentConstructor.getName());
            printLine("Current constructor's type: " + Arrays.toString(currentConstructor.getParameterTypes()));
        }

        fillConsole();

        final DateTime dateTime = new DateTime();
        final Field privateLocalDateTime = dateTime.getClass().getDeclaredField("privateLocalDateTime");
        privateLocalDateTime.setAccessible(true);
        // privateLocalDateTime.set(this, LocalDateTime.now());
        printLine("Accessible field (deprecated): " + privateLocalDateTime.isAccessible());
    }
}
