package com.playground.java.eight;
import com.playground.utils.Shipment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import static com.playground.utils.Utils.fillConsole;
import static com.playground.utils.Utils.printLine;

public class LambdaMethodReference {

    public static void main(String[] args) {

        final List<String> orderedStrListByLength = Arrays.asList("First", "Second", "Third");
        orderedStrListByLength.sort(Comparator.comparingInt(String::length));
        printLine(orderedStrListByLength);

        fillConsole();

        var orderedStrListNaturally = orderedStrListByLength;
        orderedStrListNaturally.sort(Comparator.naturalOrder());
        printLine(orderedStrListNaturally);

        fillConsole();

        final List<Shipment> shipments = Arrays.asList(new Shipment(5.0), new Shipment(25.3));
        Function<Shipment, Double> shipmentsFunction = shipment -> shipment.calculateWeight();
        printLine(calculateOnShipments(shipments, shipmentsFunction));
        shipmentsFunction = Shipment::calculateWeight;
        printLine(calculateOnShipments(shipments, shipmentsFunction));
    }

    public static List<Double> calculateOnShipments(List<Shipment> shipments, Function<Shipment, Double> function) {
        final List<Double> results = new ArrayList<>();
        for(final Shipment currentShipment : shipments) {
            results.add(function.apply(currentShipment));
        }
        return results;
    }

}
