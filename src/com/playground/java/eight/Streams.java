package com.playground.java.eight;

import com.playground.java.eight.model.Computer;
import com.playground.java.eight.model.Soundcard;
import com.playground.java.eight.model.USB;
import com.playground.utils.Shipment;

import java.util.*;
import java.util.function.Function;
import java.util.stream.*;
import static com.playground.utils.Utils.fillConsole;
import static com.playground.utils.Utils.printLine;

public class Streams {

    public static void main(String[] args) {

        // Instantiation
        Arrays.asList("a1", "a2", "a3").stream().findAny().ifPresent(System.out::println);
        Stream.of("a1", "a2", "a3").findFirst().ifPresent(System.out::println);
        IntStream.rangeClosed(2, 10).forEach(System.out::println);
        DoubleStream.of(2.12, 6.19, 7.40).average().ifPresent(System.out::println);
        Stream.of(2.12, 6.19, 7.40).mapToInt(Double::intValue).max().getAsInt();
        LongStream.of(10, 20, 30).mapToObj(number -> number + "test").forEach(System.out::println);

        fillConsole();

        // Collecting
        List<String> stringsList = Arrays.asList("A", "B", "c", "");
        stringsList = stringsList.stream().filter(string -> !string.isBlank()).collect(Collectors.toList());
        printLine(stringsList);

        fillConsole();

        final List<Shipment> shipments = Arrays.asList(new Shipment(5.0), new Shipment(25.3),
                new Shipment(25.3));
        final Map<Double, List<Shipment>> shipmentsByWeight
                = shipments.stream().collect(Collectors.groupingBy(Shipment::getWeight));
        shipmentsByWeight.forEach((weight, shipment) -> System.out.format("weight %4.3f: %s", weight, shipment));

        fillConsole();

        final Double shipmentsAvgWeight = shipments.stream().collect(Collectors.averagingDouble(Shipment::getWeight));
        printLine("Average shipments weight: " + shipmentsAvgWeight);

        fillConsole();

        // Mapping
        final String phrase = shipments.stream().map(Shipment::getWeight).map(Object::toString)
                .collect(Collectors.joining(", ", "Shipments with weights ", " are positive!"));
        printLine(phrase);

        fillConsole();

        final Map<Shipment, Double> shipmentsMap = shipments.stream()
                .collect(Collectors.toMap(Function.identity(), Shipment::getWeight));
        printLine(shipmentsMap);

        fillConsole();

        final Double reduce = shipments.stream().map(Shipment::getWeight)
                .reduce(0.0, Double::sum); // (subtotal, element) -> subtotal + element)
        printLine(reduce);

        fillConsole();

        final Double parallelizedReduce = shipments.parallelStream().map(Shipment::getWeight)
                .reduce(0.0, (subtotal, element) ->
                        subtotal + element, Double::sum); // (subtotal, element) -> subtotal + element)
        printLine(parallelizedReduce);

        fillConsole();

        // Flatmapping
        final List<Integer> firstList = Arrays.asList(1, 2, 3);
        final List<Integer> secondList = Arrays.asList(4, 5, 6);
        final List<Integer> thirdList = Arrays.asList(7, 8, 9);
        final List<List<Integer>> listOfMultipleLists = Arrays.asList(firstList, secondList, thirdList);
        final List<Integer> flattenedList = listOfMultipleLists.stream().flatMap(Collection::stream).collect(Collectors.toList());
        printLine(flattenedList);

        fillConsole();

        final USB usb = new USB();
        usb.setVersion("3.0");
        final Soundcard soundCard = new Soundcard();
        soundCard.setUsb(Optional.of(usb));
        final Computer computer = new Computer();
        computer.setSoundcard(Optional.of(soundCard));

        final Optional<Computer> computerOptional = Optional.of(computer);
        final String usbVersion = computerOptional
                .flatMap(Computer::getSoundcard)
                .flatMap(Soundcard::getUsb)
                .map(USB::getVersion).orElse("N/A");
        printLine(usbVersion);
    }
}
