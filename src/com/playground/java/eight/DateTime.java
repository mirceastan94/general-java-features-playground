package com.playground.java.eight;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.Set;

import static com.playground.utils.Utils.fillConsole;
import static com.playground.utils.Utils.printLine;

public class DateTime {

    private LocalDateTime privateLocalDateTime = LocalDateTime.now();

    public static void main(String[] args) {

        // General
        final LocalDate localDate = LocalDate.now();
        printLine(localDate.plusDays(2).getDayOfYear());
        printLine(localDate.minus(3, ChronoUnit.MONTHS));

        fillConsole();

        final LocalDateTime parsedLocalDateTime = LocalDateTime.parse("2020-07-20T09:15:00");
        printLine(parsedLocalDateTime);

        final LocalDateTime ofLocalDateTime = LocalDateTime.of(2020, Month.JULY, 20, 9, 15);
        printLine(ofLocalDateTime);

        fillConsole();

        // Zoned
        final Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();
        printLine(availableZoneIds);

        fillConsole();

        final ZoneId zoneId = ZoneId.of("Europe/London");
        final ZonedDateTime zonedDateTime = ZonedDateTime.of(parsedLocalDateTime, zoneId);
        printLine(zonedDateTime);
        printLine(zonedDateTime.toLocalTime());

        final ZoneOffset zoneOffset = ZoneOffset.ofHours(2);
        final OffsetDateTime offsetDateTime = OffsetDateTime.of(ofLocalDateTime, zoneOffset);
        printLine(offsetDateTime);

        // Period
        final LocalDate initialDate = LocalDate.parse("2007-05-10");
        final LocalDate updatedDate = initialDate.plus(Period.ofYears(10));
        printLine(updatedDate);
        int yearsDifference = Period.between(initialDate, updatedDate).getYears();
        printLine(yearsDifference);
        long daysDifference = ChronoUnit.DAYS.between(initialDate, updatedDate);
        printLine(daysDifference);

        // Duration
        final LocalTime initialTime = LocalTime.of(7, 45, 0);
        final LocalTime finalTime = initialTime.plus(Duration.ofMinutes(18));
        printLine(finalTime);
        final long durationDiffInSeconds = Duration.between(initialTime, finalTime).getSeconds();
        printLine(durationDiffInSeconds);
        final long durationDiffInMinutes = ChronoUnit.MINUTES.between(initialTime, finalTime);
        printLine(durationDiffInMinutes);

        // Formatting
        final LocalDateTime anotherLocalDateTime = LocalDateTime.of(localDate, initialTime);
        printLine(anotherLocalDateTime);
        final String localDateStr = anotherLocalDateTime.format(DateTimeFormatter.ISO_DATE);
        printLine(localDateStr);
        final String patternLocalDateStr = anotherLocalDateTime.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        printLine(patternLocalDateStr);
        final String localizedDateStr = anotherLocalDateTime.format(DateTimeFormatter
                .ofLocalizedDateTime(FormatStyle.SHORT).withLocale(Locale.CANADA));
        printLine(localizedDateStr);
    }
}
