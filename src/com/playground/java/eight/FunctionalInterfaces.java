package com.playground.java.eight;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.*;

import static com.playground.utils.Utils.fillConsole;
import static com.playground.utils.Utils.printLine;

public class FunctionalInterfaces {

    public static void main(String[] args) {

        fillConsole();

        // Operators
        final UnaryOperator<String> strWithQuotesOperator = str -> "'" + str + "'";
        final UnaryOperator<String> upperCaseStrOperator = String::toUpperCase;
        final BinaryOperator<Long> multipliedValuesOperator = Math::multiplyExact;
        final String uppercaseSir = upperCaseStrOperator.apply("tesTs");
        final String quotedStr = strWithQuotesOperator.apply("Testing");
        final Long multipliedValues = multipliedValuesOperator.apply(Long.parseLong("35"),
                Long.parseLong("12"));
        printLine("Quoted string: " + quotedStr);
        printLine("Uppercased string: " + uppercaseSir);
        printLine("Multiplied numbers: " + multipliedValues);

        fillConsole();

        // Functions
        final ToIntFunction<String> intoToStrFct = Integer::parseInt;
        final BiFunction<Integer, Double, String> stringifyNumbersFct = (intNr, doubleNr) -> intNr.toString() + ", "
                + doubleNr.toString();
        final Integer convertedStr = intoToStrFct.applyAsInt("60");
        final String stringifiedNr = stringifyNumbersFct.apply(10, 3.5);
        printLine("Converted integer: " + convertedStr);
        printLine("Stringified numbers: " + stringifiedNr);

        fillConsole();

        // Predicates
        final IntPredicate lesserThanFivePred = number -> number < 5;
        final Predicate<String> longerThanTenPred = str -> str.length() > 10;
        boolean numberPredicate = lesserThanFivePred.negate().test(11);
        boolean stringPredicate = longerThanTenPred.test("LongerString");
        printLine("Number bigger than 5: " + numberPredicate);
        printLine("String longer than 10: " + stringPredicate);

        fillConsole();

        // Supplier
        final Supplier<LocalDateTime> date = LocalDateTime::now;
        printLine("Current date-time: " + date.get().toLocalDate());

        fillConsole();

        // Consumer
        final Consumer<List<String>> stringListConsumer = list -> list.stream().forEach(System.out::println);
        stringListConsumer.accept(Arrays.asList("First", "Second", "Third"));

        fillConsole();

        // (List) -> Consumer
        final List<String> stringList = new ArrayList<>();
        stringList.add("Stock");
        stringList.add("ETF");
        stringList.forEach(System.out::println);

        fillConsole();

        // (Map) -> BiFunction, BiConsumer
        final Map<String, Double> exampleMap = new LinkedHashMap<>();
        exampleMap.put("Squared even number", (double) 10);
        exampleMap.put("Halved uneven number", (double) 9);
        exampleMap.compute("Squared even number", (key, val) -> Math.pow(val, 2));
        exampleMap.compute("Halved uneven number", (key, val) -> val / 2);
        exampleMap.forEach((key, value) -> printLine(key + " : " + value));
    }
}
