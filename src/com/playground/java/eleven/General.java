package com.playground.java.eleven;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.playground.utils.Utils.fillConsole;
import static com.playground.utils.Utils.printLine;

public class General {

    public static void main(String[] args) {

        final String text = "Pre-Java 10";
        printLine(text);
        final var textNew = "Java 10";
        printLine(textNew);

        fillConsole();

        var myList = List.of("Test1", "Test2", "Test3");
        myList.forEach(System.out::println);

        fillConsole();

        final Predicate<String> myPredicate = (var str) -> !str.isBlank();
        printLine("String is not empty: " + myPredicate.test("NotEmpty"));

        fillConsole();

        final var myMap = Map.of("A", 1, "B", 2);
        printLine("Contains key 'A': " + myMap.containsKey("A"));
        printLine("Contains key 'C': " + myMap.containsKey("C"));

        fillConsole();

        final Supplier<IntStream> range = () -> IntStream.range(0, 10);
        printLine("Stream count: " + range.get().count());

        fillConsole();

        final var lowerThanSevenList = range.get().takeWhile(number -> number < 7)
                .boxed().collect(Collectors.toList());
        final var higherThanThreeList = range.get().dropWhile(number -> number < 5)
                .boxed().collect(Collectors.toList());
        printLine(lowerThanSevenList);
        printLine(higherThanThreeList);

        fillConsole();

        final var testFromOptStr = Optional.of("Test").orElseThrow(NullPointerException::new);
        printLine(testFromOptStr);
        final var anotherTestFromOptStr = Optional.of("Test").or(() -> Optional.of("Another test"));
        printLine(anotherTestFromOptStr.get());

        fillConsole();

        final var featuresStr = " This ia a sample text to show \n new Java 11 string methods ";
        printLine("Original String: " + featuresStr);
        printLine("String is blank: " + featuresStr.isBlank());
        printLine("Stripped String: " + featuresStr.strip());
        printLine("Repeated String: " + featuresStr.repeat(2));
        printLine("String lines count: " + featuresStr.lines().count());
    }
}
