package com.playground.java.eleven;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

public class HTTPClient {

    public static void main(String[] args) throws IOException, InterruptedException {

        var httpRequest = HttpRequest.newBuilder().uri(URI.create("https://galant.com")).GET().build();
        var client = HttpClient.newHttpClient();
        final HttpResponse<String> response = client.send(httpRequest,
                HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8));
        System.out.println(response);

    }
}
