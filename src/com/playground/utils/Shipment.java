package com.playground.utils;

public class Shipment {

    Double weight;

    public Shipment(Double weight) {
        this.weight = weight;
    }

    public double calculateWeight() {
        return this.weight * 10;
    }

    public Double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "Shipment{" +
                "weight=" + weight +
                '}';
    }
}
