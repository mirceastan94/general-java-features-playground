package com.playground.utils;

public class Utils {

    public static <T> void printLine(T obj) {
        System.out.println(obj);
    }

    public static void fillConsole() {
        printLine("");
        printLine("-----------------------------");
        printLine("");
    }
}
